



 #  使用nodejs 配合ADB 自动关注抖音小姐姐

> 起因 在github上看到一个项目 **find-beauty-in-douyin** 自动·关注抖音，
>
> 然后拉下来启动 ，本人win10环境启动不了。
>
> 参考他的项目重新配了一下

## 启动前准备工作

1. 安装 adb  ,不会的百度下

2. 打开手机开发者模式，连接手机
    方式一：通过 USB 直接连接即可
     方式二：通过 wifi 连接，参考这篇文章：[Android studio使用---WiFi ADB使用以及连接手机调试](https://blog.csdn.net/xiabing082/article/details/54376461)

3. 打开cmd输入 查看是否连接成功

   ```base
   adb devices
   ```

   [](https://segmentfault.com/img/bVbF5QU)

   其他命令可以参考[adb 使用](https://segmentfault.com/a/1190000022386559)

4. 申请face++的key 填入config/faceApi 中 **`api_key` 和 `api_secret`**

   [文档](https://console.faceplusplus.com.cn/documents/4888383)

## 启动项目


```bash
npm install
npm run dev
```

## 结果

如图 刷了350多个 关注了76人

![](./StyleImg/b.jpg)


![](./StyleImg/a.png)