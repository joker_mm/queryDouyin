const { openApp, saveFile, swipe, touch } = require('./config/adbShell')
const face = require('./config/faceApi')
const path = require('path')
// const fs = require('fs');

function awaitMoment (time = 2000) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), time)
  })
}

(async function mian () {
  // 打开APP
  await openApp()

  // 图片路径
  const rel = path.resolve(__dirname, 'img', 'screen_')

  // 自动关注
  await followFn(rel)
})()

async function followFn (rel) {
  // 保存当前截图在本地
  let filename = await saveFile()
  
  // 调用face 检测是否达到标准 
  const { shouldFollow, score } = await face(`${rel}${filename}.png`)
  console.log(shouldFollow, score)


  // 达到自动关注
  if (shouldFollow) {
    await touch()
    // await touch({ x: 510, y: 510 })
    // await touch({x: 500, y: 500})
  } else {
    // console.log('pass')
  }
  // 下一个视频
  await swipe({ x: 300, y: 1000 }, { x: 300, y: 640 })
  await awaitMoment(5000)
  
  return await followFn(rel)
}
